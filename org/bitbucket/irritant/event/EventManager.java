package org.bitbucket.irritant.event;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

public class EventManager {

	private static final String NAME = "[EventManager]";
	private static final Map<Class<?>, List<MethodStorage>> EVENT_STORAGE = new ConcurrentHashMap<Class<?>, List<MethodStorage>>();
	
	/**
	 * Fires a event to all methods that have the event as a parameter.
	 * @param eventInstance The instance of the event to be fired.
	 */
	public static synchronized void fire(final Event eventInstance) {
		handleFire(eventInstance);
	}
	
	/**
	 * Registers a class which contains event methods.
	 * @param instance The class instance to be registered.
	 */
	public static synchronized void register(final Object instance) {
		handleRegister(instance);
	}
	
	/**
	 * Unregisters a class which contains event methods.
	 * @param instance The class instance to be unregistered.
	 */
	public static synchronized void unregister(final Object instance) {
		handleUnregister(instance);
	}
	
	private static void handleFire(final Event eventInstance) {
		final Class<? extends Event> eventClass = eventInstance.getClass();
		if (EVENT_STORAGE.containsKey(eventClass)) {
			final List<MethodStorage> storage = sortList(EVENT_STORAGE.get(eventClass));

			for (Iterator<MethodStorage> iterator = storage.iterator(); iterator.hasNext();) {
				MethodStorage methodStore = iterator.next();
				final Object methodInstance = methodStore.getMethodInstance();
				final Method method = methodStore.getMethod();

				try {
					method.invoke(methodInstance, eventInstance);
				} catch (IllegalAccessException | IllegalArgumentException
						| InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private static void handleRegister(final Object instance) {
		Class<?> methodClass = instance.getClass();
		Arrays.asList(methodClass.getDeclaredMethods()).forEach(method -> {
			if(!method.isAnnotationPresent(Subscriber.class))
				return;
			if(method.getParameterAnnotations().length != 1) {
				logger(method.getName() + " does not have 1 parameter!");
				return;
			}
			
			if(!(method.getParameterTypes()[0].getClass().isInstance(Event.class))) {
				logger(method.getName() + " does not have a event parameter!");
				return;
			}

			Class<?> event = (Class<?>) method.getParameterTypes()[0];
			
			if(!method.isAccessible())
				method.setAccessible(true);
			
			if(EVENT_STORAGE.containsKey(event)) {
				List<MethodStorage> storage = EVENT_STORAGE.get(event);
				storage.add(new MethodStorage(instance, method, method.getAnnotation(Subscriber.class)));
				logger("[REGISTERED+]: " + methodClass.getName() + " - " + method.getName());
			} else {
				List<MethodStorage> storage = new ArrayList<MethodStorage>();
				storage.add(new MethodStorage(instance, method, method.getAnnotation(Subscriber.class)));
				EVENT_STORAGE.put(event, storage);
				logger("[REGISTERED]: " + methodClass.getName() + " - " + method.getName());
			}
		});
	}
	
	private static void handleUnregister(final Object instance) {
		for(Entry<Class<?>, List<MethodStorage>> entry : EVENT_STORAGE.entrySet()) {
			List<MethodStorage> storageList = entry.getValue();
			for (Iterator<MethodStorage> iterator = storageList.iterator(); iterator.hasNext();) {
				MethodStorage method = iterator.next();
				if(method.methodClass.equals(instance)) {
					iterator.remove();
				}
			}
		}
	}
	
	private synchronized static List<MethodStorage> sortList(List<MethodStorage> storage) {
		List<MethodStorage> tempStorage = storage;
		Collections.sort(tempStorage);
		return tempStorage;
	}
	
	private static void logger(Object... object) {
		for(Object obj : object) {
			System.out.println(NAME + ": " + obj);
		}
	}
	
	private static class MethodStorage implements Comparable<MethodStorage> {
		
		private Object methodClass;
		private Method method;
		private Subscriber annotation;
		
		public MethodStorage(Object methodClass, Method method, Subscriber annotation) {
			this.methodClass = methodClass;
			this.method = method;
			this.annotation = annotation;
		}

		public Object getMethodInstance() {
			return methodClass;
		}
		
		public Method getMethod() {
			return method;
		}
		
		public Subscriber getAnnotation() {
			return this.annotation;
		}

		@Override
		public int compareTo(MethodStorage o) {
			byte b1 = o.getAnnotation().priority().getValue();
			byte b2 = this.getAnnotation().priority().getValue();
			
			return b1 - b2;
		}
	}
}
