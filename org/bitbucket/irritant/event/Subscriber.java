package org.bitbucket.irritant.event;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Subscriber {
	
	EventPriority priority() default EventPriority.MEDIUM;
	
	public enum EventPriority {
		LOW, MEDIUM, HIGH;
		
		public byte getValue() {
			switch(this) {
			case LOW:
				return 0;
			case MEDIUM:
				return 1;
			case HIGH:
				return 2;
			}
			return 2;
		}
	}
}
