package org.bitbucket.irritant.event;

public class EventCancellable implements Event {

	private boolean isCancelled;
	
	public boolean isCancelled() {
		return isCancelled;
	}
	
	public void setCancelled(final boolean state) {
		this.isCancelled = state;
	}
}
