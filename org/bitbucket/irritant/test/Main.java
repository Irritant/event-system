package org.bitbucket.irritant.test;

import org.bitbucket.irritant.event.EventManager;
import org.bitbucket.irritant.event.Subscriber;
import org.bitbucket.irritant.test.events.EventTest;

public class Main {

	static final Main main = new Main();
	
	public static void main(String[] args) {
		EventManager.register(main);
		EventManager.fire(new EventTest());
	}

	@Subscriber
	public void onEventTest(EventTest event) {
		System.out.println("EventTest has been received.");
	}
}
